package org.jwierzbicki.task.account;


class BalanceMapper {

    public AccountService.Balances toDomain(BalancesEntity entity) {
        var balancesList = entity.getBalances().entrySet().stream()
                .map(e -> new AccountService.Balance(e.getKey(), e.getValue().getAvailableBalance(), e.getValue().getLockedBalance()))
                .toList();
        return new AccountService.Balances(balancesList);
    }

    public AccountService.Balance toDomain(BalanceEntity entity) {
        return new AccountService.Balance(entity.getCurrency(), entity.getAvailableBalance(), entity.getLockedBalance());
    }

}
