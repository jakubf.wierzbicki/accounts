package org.jwierzbicki.task.account;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class BalancesEntity {

    private Map<String, BalanceEntity> balances;

    public BalancesEntity() {
        this.balances = new HashMap<>();
    }

    public Map<String, BalanceEntity> getBalances() {
        return balances;
    }

    public boolean hasCurrency(String currency) {
        return balances.containsKey(currency);
    }

    public void setAvailableBalance(String currency, BigDecimal balance) {
        balances.computeIfAbsent(currency, c -> new BalanceEntity(currency, BigDecimal.ZERO, BigDecimal.ZERO)).setAvailableBalance(balance);
    }

    public void setLockedBalance(String currency, BigDecimal balance) {
        balances.computeIfAbsent(currency, c -> new BalanceEntity(currency, BigDecimal.ZERO, BigDecimal.ZERO)).setLockedBalance(balance);
    }

    public BigDecimal getAvailableBalance(String currency) {
        if (balances.containsKey(currency)) {
            return balances.get(currency).getAvailableBalance();
        } else {
            throw new IllegalArgumentException("Currency not found");
        }
    }

    public BigDecimal getLockedBalance(String currency) {
        if (balances.containsKey(currency)) {
            return balances.get(currency).getLockedBalance();
        } else {
            throw new IllegalArgumentException("Currency not found");
        }
    }



}
