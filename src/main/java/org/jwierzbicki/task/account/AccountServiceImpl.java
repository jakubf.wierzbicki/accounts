package org.jwierzbicki.task.account;

import org.jwierzbicki.task.exceptions.AccountNotFound;
import org.jwierzbicki.task.Common;
import org.jwierzbicki.task.Common.*;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class AccountServiceImpl implements AccountService {

    private final Map<Common.AccountId, AccountEntity> accounts = new ConcurrentHashMap<>();

    private final BalanceMapper balanceMapper = new BalanceMapper();


    @Override
    public Balances getBalance(AccountId accountId) {
        var account = getAccEntity(accountId);
        synchronized (account) {
            return balanceMapper.toDomain(account.getBalances());
        }
    }

    @Override
    public AccountId createAccount() {
        UUID accountUuid = UUID.randomUUID();
        AccountId accountId = new AccountId(accountUuid.toString());
        var previous = accounts.putIfAbsent(accountId, new AccountEntity(accountUuid.toString()));
        if(previous != null) {
            throw new IllegalStateException("Account already exists");
        }
        return accountId;
    }

    @Override
    public boolean accountExists(AccountId accountId) {
        return accounts.containsKey(accountId);
    }

    @Override
    public void addFunds(AccountId accountId, Common.CurrencyAmount amount) {
        AccountEntity account = getAccEntity(accountId);
        synchronized (account) {
            var balance = account.getBalances();
            var hasCurrency = balance.hasCurrency(amount.currency());
            if(hasCurrency) {
                BigDecimal added = balance.getAvailableBalance(amount.currency()).add(amount.value());
                balance.setAvailableBalance(amount.currency(), added);
            } else {
                balance.setAvailableBalance(amount.currency(), amount.value());
            }
        }
    }


    private AccountEntity getAccEntity(AccountId accountId) {
        return Optional.ofNullable(accounts.get(accountId))
                .orElseThrow(() -> new AccountNotFound(accountId.value()));
    }

    @Override
    public void transferLockedFunds(AccountId from, AccountId to, Common.CurrencyAmount amount) {
        AccountEntity fromAccount = getAccEntity(from);
        AccountEntity toAccount = getAccEntity(to);
        if(fromAccount.equals(toAccount)) {
            throw new IllegalArgumentException("Cannot transfer funds to the same account");
        }
        if(from.value().compareTo(to.value()) < 0) {
            synchronized (fromAccount) {
                synchronized (toAccount) {
                    transferFunds(from, to, amount);
                }
            }
        } else {
            synchronized (toAccount) {
                synchronized (fromAccount) {
                    transferFunds(from, to, amount);
                }
            }
        }

    }

    @Override
    public void lockBalance(AccountId accountId, Common.CurrencyAmount amount) {
        AccountEntity account = getAccEntity(accountId);
        synchronized (account) {
            var balances = account.getBalances();
            var hasBalance = balances.hasCurrency(amount.currency());
            if(!hasBalance) {
                throw new IllegalArgumentException("Not enough funds");
            } else {
                var availableBalance = balances.getAvailableBalance(amount.currency()).subtract(balances.getLockedBalance(amount.currency()));
                if(availableBalance.compareTo(amount.value()) < 0) {
                    throw new IllegalArgumentException("Not enough funds");
                }
                balances.setLockedBalance(amount.currency(), balances.getLockedBalance(amount.currency()).add(amount.value()));
                balances.setAvailableBalance(amount.currency(), balances.getAvailableBalance(amount.currency()).subtract(amount.value()));
            }
        }
    }

    @Override
    public void unlockBalance(AccountId accountId, Common.CurrencyAmount amount) {
        AccountEntity account = getAccEntity(accountId);
        synchronized (account) {
            var balances = account.getBalances();
            var hasBalance = balances.hasCurrency(amount.currency());
            if(!hasBalance) {
                throw new IllegalArgumentException("Currency not found");
            } else {
                var lockedBalance = balances.getLockedBalance(amount.currency());
                if(lockedBalance.compareTo(amount.value()) < 0) {
                    throw new IllegalArgumentException("Not enough funds");
                }
                balances.setLockedBalance(amount.currency(), lockedBalance.subtract(amount.value()));
                balances.setAvailableBalance(amount.currency(), balances.getAvailableBalance(amount.currency()).add(amount.value()));
            }
        }
    }

    @Override
    public void removeLockedFunds(AccountId accountId, Common.CurrencyAmount amount) {
        AccountEntity account = getAccEntity(accountId);
        synchronized (account) {
            var balances = account.getBalances();
            var hasBalance = balances.hasCurrency(amount.currency());
            if(!hasBalance) {
                throw new IllegalArgumentException("Not enough funds");
            } else {
                var lockedBalance = balances.getLockedBalance(amount.currency());
                if(lockedBalance.compareTo(amount.value()) < 0) {
                    throw new IllegalArgumentException("Not enough funds");
                }
                balances.setLockedBalance(amount.currency(), lockedBalance.subtract(amount.value()));
            }
        }
    }

    private void transferFunds(AccountId fromAccount, AccountId toAccount, Common.CurrencyAmount amount) {
        removeLockedFunds(fromAccount, amount);
        addFunds(toAccount, amount);
    }



}
