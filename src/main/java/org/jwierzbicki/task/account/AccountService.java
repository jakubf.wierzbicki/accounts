package org.jwierzbicki.task.account;

import org.jwierzbicki.task.exceptions.AccountNotFound;
import org.jwierzbicki.task.Common;
import org.jwierzbicki.task.Common.*;

import java.math.BigDecimal;
import java.util.List;

public interface AccountService {

    /**
     * Get the availableBalance for a given account
     *
     * @param accountId
     * @return the balances
     * @throws AccountNotFound if the account is not found
     */
    Balances getBalance(AccountId accountId);

    /**
     * Create a new account
     *
     * @return the account id
     * @throws IllegalStateException if the account already exists
     */
    AccountId createAccount();

    /**
     * Check if the account exists
     *
     * @param accountId
     * @return true if the account exists
     */
    boolean accountExists(AccountId accountId);

    /**
     * Add funds to the account
     *
     * @param accountId
     * @param amount
     * @throws AccountNotFound if the account is not found
     */
    void addFunds(AccountId accountId, Common.CurrencyAmount amount);

    /**
     * Transfer funds between accounts
     *
     * @param from
     * @param to
     * @param amount
     * @throws IllegalArgumentException if the amount is greater than the locked availableBalance or if the currency is not found
     * or if the accounts are the same
     * @throws AccountNotFound if one of the accounts is not found
     */
    void transferLockedFunds(AccountId from, AccountId to, Common.CurrencyAmount amount);

    /**
     * Lock availableBalance for a given account
     *
     * @param accountId
     * @param amount
     * @throws IllegalArgumentException if the amount is greater than the available availableBalance or if the currency is not found
     * @throws AccountNotFound if the account is not found
     */
    void lockBalance(AccountId accountId, Common.CurrencyAmount amount);

    /**
     * Unlock availableBalance that was previously locked
     *
     * @param accountId
     * @param amount
     * @throws IllegalArgumentException if the amount is greater than the locked availableBalance or if the currency is not found
     * @throws AccountNotFound if the account is not found
     */
    void unlockBalance(AccountId accountId, Common.CurrencyAmount amount);

    /**
     * Release locked availableBalance that was previously locked
     *
     * @param accountId
     * @param amount
     * @throws IllegalArgumentException if the amount is greater than the locked availableBalance or if the currency is not found
     * @throws AccountNotFound if the account is not found
     */
    void removeLockedFunds(AccountId accountId, Common.CurrencyAmount amount);

    record Balance(String currency, BigDecimal availableBalance, BigDecimal lockedBalance) {}
    record Balances(List<Balance> balances) {}

}
