package org.jwierzbicki.task.account;

import java.math.BigDecimal;

public class BalanceEntity {

    String currency;
    BigDecimal availableBalance;
    BigDecimal lockedBalance;

    public BalanceEntity(String currency, BigDecimal availableBalance, BigDecimal lockedBalance) {
        this.currency = currency;
        this.availableBalance = availableBalance;
        this.lockedBalance = lockedBalance;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(BigDecimal availableBalance) {
        this.availableBalance = availableBalance;
    }

    public BigDecimal getLockedBalance() {
        return lockedBalance;
    }

    public void setLockedBalance(BigDecimal lockedBalance) {
        this.lockedBalance = lockedBalance;
    }


}
