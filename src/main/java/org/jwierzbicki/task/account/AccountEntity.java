package org.jwierzbicki.task.account;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

class AccountEntity {

    private String accountId;
    private BalancesEntity balances;

    public AccountEntity() {
    }

    public AccountEntity(String accountId) {
        this.accountId = accountId;
        this.balances = new BalancesEntity();
    }


    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public BalancesEntity getBalances() {
        return balances;
    }

}
