package org.jwierzbicki.task;

import org.jwierzbicki.task.withdrawal.WithdrawalService;

import java.math.BigDecimal;

public class Common {

    public record AccountId(String value) {}

    public record Withdrawal(AccountId accountId, WithdrawalService.Address address, CurrencyAmount amount, Common.WithdrawalState withdrawalState) {
        public Withdrawal copyWithState(Common.WithdrawalState state) {
            return new Withdrawal(accountId, address, amount, state);
        }
    }

    public enum WithdrawalState {
        PENDING, PROCESSING, FAILED, SUCCESS
    }

    public record CurrencyAmount(BigDecimal value, String currency) {
    }
}
