package org.jwierzbicki.task.withdrawal;

import org.jwierzbicki.task.account.AccountService;
import org.jwierzbicki.task.Common;

import java.util.UUID;
import java.util.concurrent.*;

public class AsyncWithdrawalClientWrapper {

    private final AccountService accountService;
    private final ConcurrentMap<WithdrawalService.WithdrawalId, Common.Withdrawal> withdrawalRepository = new ConcurrentHashMap<>();
    private final WithdrawalService withdrawalService;
    private final ScheduledExecutorService executorService;

    public AsyncWithdrawalClientWrapper(AccountService accountService,
                                        WithdrawalService withdrawalService,
                                        ScheduledExecutorService executorService) {
        this.accountService = accountService;
        this.withdrawalService = withdrawalService;
        this.executorService = executorService;
    }

    public void start() {
        scheduleWithdrawalsUpdates();
    }

    public WithdrawalService.WithdrawalId requestWithdrawal(WithdrawalService.WithdrawalId withdrawalId, Common.AccountId accountId, WithdrawalService.Address address, Common.CurrencyAmount amount) {
        if(!accountService.accountExists(accountId)) {
            throw new IllegalArgumentException("Account %s does not exist".formatted(accountId.value()));
        }
        Common.Withdrawal withdrawal = new Common.Withdrawal(accountId, address, amount, Common.WithdrawalState.PENDING);
        var previous = withdrawalRepository.putIfAbsent(withdrawalId, withdrawal);
        if (previous != null) {
            throw new IllegalStateException("Withdrawal request with id[%s] is already present".formatted(withdrawalId.value()));
        }
        try {
            accountService.lockBalance(accountId, amount);
        } catch(Exception ex) {
            withdrawalRepository.computeIfPresent(withdrawalId, (id, w) -> w.copyWithState(Common.WithdrawalState.FAILED));
            return withdrawalId;
        }

        try {
            withdrawalService.requestWithdrawal(withdrawalId, address, amount);
        } catch (Exception ex) {
            System.out.println("Withdrawal request failed");
        }
        return withdrawalId;
    }

    public Common.WithdrawalState getRequestState(WithdrawalService.WithdrawalId id) {
        if(!withdrawalRepository.containsKey(id)) {
            throw new IllegalArgumentException("Request %s is not found".formatted(id.value()));
        }
        return withdrawalRepository.get(id).withdrawalState();
    }


    private void scheduleWithdrawalsUpdates() {
        executorService.scheduleAtFixedRate(this::updateStates, 0, 1, java.util.concurrent.TimeUnit.SECONDS);
    }

    private void updateStates() {
        withdrawalRepository.forEach((id, withdrawal) -> {
            if(!isWithdrawalFinished(withdrawal)) {
                WithdrawalService.WithdrawalId externalWithdrawId = getExternalWithdrawId(id.value());
                try {
                    WithdrawalService.WithdrawalState externalWithdrawState = withdrawalService.getRequestState(externalWithdrawId);
                    if (externalWithdrawState == WithdrawalService.WithdrawalState.COMPLETED) {
                        finishWithdrawal(id, Common.WithdrawalState.SUCCESS);
                    } else if (externalWithdrawState == WithdrawalService.WithdrawalState.FAILED) {
                        finishWithdrawal(id, Common.WithdrawalState.FAILED);
                    }
                } catch (IllegalArgumentException ex) {
                    // no withdrawal found
                    finishWithdrawal(id, Common.WithdrawalState.FAILED);
                } catch (Exception ex) {
                    // log exception
                }
            }
        });
    }

    //transactional?
    private void finishWithdrawal(WithdrawalService.WithdrawalId withdrawalId, Common.WithdrawalState success) {
        System.out.println("Finishing withdrawal %s".formatted(withdrawalId.value()));
        if(success == Common.WithdrawalState.SUCCESS) {
            try {
                var updated = withdrawalRepository.get(withdrawalId);
                if(updated != null) {
                    accountService.removeLockedFunds(updated.accountId(), updated.amount());
                    withdrawalRepository.put(withdrawalId,updated.copyWithState(Common.WithdrawalState.SUCCESS));
                    System.out.println("Withdrawal %s finished".formatted(withdrawalId.value()));
                } else {
                    // log error
                }
            } catch (IllegalArgumentException ex) {
                // log error
                //leave withdrawal in pending state to check later
            }
        } else if (success == Common.WithdrawalState.FAILED) {
            try {
                var updated = withdrawalRepository.get(withdrawalId);
                if(updated != null) {
                    accountService.unlockBalance(updated.accountId(), updated.amount());
                    withdrawalRepository.put(withdrawalId,updated.copyWithState(Common.WithdrawalState.FAILED));
                    System.out.println("Withdrawal %s failed".formatted(withdrawalId.value()));
                } else {
                    // log error
                }
            } catch (Exception ex) {
                // log error
            }
        } else {
            // unexpected state
        }
    }

    private static WithdrawalService.WithdrawalId getExternalWithdrawId(UUID id) {
        return new WithdrawalService.WithdrawalId(id);
    }

    private static boolean isWithdrawalFinished(Common.Withdrawal withdrawal) {
        return withdrawal.withdrawalState() == Common.WithdrawalState.SUCCESS || withdrawal.withdrawalState() == Common.WithdrawalState.FAILED;
    }


}
