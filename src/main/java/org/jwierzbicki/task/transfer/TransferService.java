package org.jwierzbicki.task.transfer;

import org.jwierzbicki.task.Common;

import java.util.UUID;

public interface TransferService {

    void requestTransfer(TransferId transferId, Common.AccountId from, Common.AccountId to, Common.CurrencyAmount amount);

    TransferState getRequestState(TransferId id);

    enum TransferState {
        PENDING, PROCESSING, SUCCESS, FAILURE
    }

    record TransferId(UUID value) {}

    record Transfer(Common.AccountId from, Common.AccountId to, Common.CurrencyAmount amount, TransferState transferState) {
        public Transfer copyWithState(TransferState state) {
            return new Transfer(from, to, amount, state);
        }
    }

}
