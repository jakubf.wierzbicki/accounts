package org.jwierzbicki.task.transfer;

import org.jwierzbicki.task.Common;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.random.RandomGenerator;

import static org.jwierzbicki.task.transfer.TransferService.TransferState.*;

public class TransferServiceStub implements TransferService {

    private final ConcurrentMap<TransferId, Transfer> requests = new ConcurrentHashMap<>();

    @Override
    public void requestTransfer(TransferId transferId, Common.AccountId from, Common.AccountId to, Common.CurrencyAmount amount) {
        final var existing = requests.putIfAbsent(transferId, new Transfer(finalState(), finaliseAt(), from, to, amount));
        if (existing != null) {
            throw new IllegalStateException("Withdrawal request with id[%s] is already present".formatted(transferId));
        }
    }

    private TransferState finalState() {
        return RandomGenerator.getDefault().nextInt(100) > 5 ? SUCCESS : FAILURE;
    }

    private long finaliseAt() {
        return System.currentTimeMillis() + ThreadLocalRandom.current().nextLong(100, 1000);
    }

    @Override
    public TransferState getRequestState(TransferId id) {
        final var request = requests.get(id);
        if (request == null)
            throw new IllegalArgumentException("Request %s is not found".formatted(id));
        return request.finalState();
    }

    record Transfer(TransferState state, long finaliseAt, Common.AccountId from, Common.AccountId to, Common.CurrencyAmount amount) {
        public TransferState finalState() {
            return finaliseAt <= System.currentTimeMillis() ? state : PROCESSING;
        }
    }
}

