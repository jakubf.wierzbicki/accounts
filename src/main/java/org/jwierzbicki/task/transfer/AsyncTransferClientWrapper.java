package org.jwierzbicki.task.transfer;

import org.jwierzbicki.task.account.AccountService;
import org.jwierzbicki.task.Common;
import org.jwierzbicki.task.transfer.TransferService.TransferId;
import org.jwierzbicki.task.transfer.TransferService.TransferState;

import java.util.concurrent.*;

public class AsyncTransferClientWrapper {

    private final AccountService accountService;
    private final ConcurrentMap<TransferId, TransferService.Transfer> transferRepository = new ConcurrentHashMap<>();
    private final TransferService transferService;
    private final ScheduledExecutorService executorService;

    public AsyncTransferClientWrapper(AccountService accountService,
                                      TransferService transferService,
                                      ScheduledExecutorService executorService) {
        this.accountService = accountService;
        this.transferService = transferService;
        this.executorService = executorService;
    }

    public void start() {
        scheduleTransfersUpdates();
    }


    public TransferId requestTransfer(TransferId transferId, Common.AccountId fromAccountId, Common.AccountId toAccountId, Common.CurrencyAmount amount) {
        System.out.println("Requesting transfer %s".formatted(transferId.value()));
        if(!accountService.accountExists(fromAccountId)) {
            throw new IllegalArgumentException("Account %s does not exist".formatted(fromAccountId.value()));
        }
        if(!accountService.accountExists(toAccountId)) {
            throw new IllegalArgumentException("Account %s does not exist".formatted(toAccountId.value()));
        }
        TransferService.Transfer transfer = new TransferService.Transfer(fromAccountId, toAccountId, amount, TransferState.PENDING);
        var previous = transferRepository.putIfAbsent(transferId, transfer);
        if (previous != null) {
            throw new IllegalStateException("Transfer request with id[%s] is already present".formatted(transferId.value()));
        }
        try {
            accountService.lockBalance(fromAccountId, amount);
        } catch(Exception ex) {
            transferRepository.computeIfPresent(transferId, (id, t) -> t.copyWithState(TransferState.FAILURE));
            return transferId;
        }
        transferService.requestTransfer(transferId, fromAccountId, toAccountId, amount);
        System.out.println("Transfer request sent %s".formatted(transferId.value()));
        return transferId;
    }

    public TransferState getRequestState(TransferId id) {
        if(!transferRepository.containsKey(id)) {
            throw new IllegalArgumentException("Transfer request %s is not found".formatted(id.value()));
        }
        return transferRepository.get(id).transferState();
    }

    private void scheduleTransfersUpdates() {
        executorService.scheduleAtFixedRate(this::updateStates, 0, 1, TimeUnit.SECONDS);
    }

    private void updateStates() {
        transferRepository.forEach((id, transfer) -> {
            if (!isTransferFinished(transfer)) {
                try {
                    TransferState transferState = transferService.getRequestState(id);
                    finishTransfer(id, transferState);
                } catch (IllegalArgumentException ex) {
                    // No transfer found, considering it failed.
                    finishTransfer(id, TransferState.FAILURE);
                } catch (Exception ex) {
                    // Log exception
                }
            }
        });
    }

    //transactional?
    private void finishTransfer(TransferId transferId, TransferState state) {
        System.out.println("Finishing transfer %s".formatted(transferId.value()));
        if(state == TransferState.SUCCESS) {
            try {
                var updated = transferRepository.get(transferId);
                if(updated != null) {
                    accountService.transferLockedFunds(updated.from(), updated.to(), updated.amount());
                    transferRepository.put(transferId, updated.copyWithState(TransferState.SUCCESS));
                    System.out.println("Transfer %s finished".formatted(transferId.value()));
                } else {
                    // log error
                }
            } catch (Exception ex) {
                // log error
            }
        } else if(state == TransferState.FAILURE) {
            var updated = transferRepository.get(transferId);
            try {
                if(updated != null) {
                    accountService.unlockBalance(updated.from(), updated.amount());
                    transferRepository.put(transferId, updated.copyWithState(TransferState.FAILURE));
                    System.out.println("Transfer %s failed".formatted(transferId.value()));
                } else {
                    // log error
                }
            } catch (Exception ex) {
                // log error
            }
        } else {
            // unexpected state
            // log error
        }
    }

    private static boolean isTransferFinished(TransferService.Transfer transfer) {
        return transfer.transferState() == TransferService.TransferState.SUCCESS || transfer.transferState() == TransferState.FAILURE;
    }

}
