package org.jwierzbicki.task.management;

import org.jwierzbicki.task.Common;
import org.jwierzbicki.task.account.AccountService;
import org.jwierzbicki.task.deposit.DepositService;
import org.jwierzbicki.task.Common.*;
import org.jwierzbicki.task.transfer.TransferService;
import org.jwierzbicki.task.withdrawal.WithdrawalService;

public interface AccountManagementService {

    AccountId createAccount();

    DepositService.DepositId deposit(DepositService.DepositId depositId, AccountId accountId, Common.CurrencyAmount amount);

    DepositService.DepositState getDepositState(DepositService.DepositId id);

    WithdrawalService.WithdrawalId withdraw(WithdrawalService.WithdrawalId withdrawalId, AccountId accountId, WithdrawalService.Address address, Common.CurrencyAmount amount);

    Common.WithdrawalState getWithdrawalState(WithdrawalService.WithdrawalId id);

    AccountService.Balances getBalances(AccountId accountId);

    TransferService.TransferId requestTransfer(TransferService.TransferId transferId, AccountId from, AccountId to, Common.CurrencyAmount amount);

    TransferService.TransferState getTransferState(TransferService.TransferId id);

    void start();


}
