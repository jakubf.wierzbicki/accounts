package org.jwierzbicki.task.management;

import org.jwierzbicki.task.Common;
import org.jwierzbicki.task.account.AccountService;
import org.jwierzbicki.task.deposit.AsyncDepositClientWrapper;
import org.jwierzbicki.task.deposit.DepositService;
import org.jwierzbicki.task.transfer.AsyncTransferClientWrapper;
import org.jwierzbicki.task.transfer.TransferService;
import org.jwierzbicki.task.withdrawal.AsyncWithdrawalClientWrapper;
import org.jwierzbicki.task.withdrawal.WithdrawalService;

import java.util.stream.Collectors;

public class AccountManagementServiceImpl implements AccountManagementService {

    private final AsyncDepositClientWrapper asyncDepositClientWrapper;
    private final AsyncTransferClientWrapper asyncTransferClientWrapper;
    private final AsyncWithdrawalClientWrapper asyncWithdrawalClientWrapper;
    private final AccountService accountService;

    public AccountManagementServiceImpl(AsyncDepositClientWrapper asyncDepositClientWrapper,
                                        AsyncTransferClientWrapper asyncTransferClientWrapper,
                                        AsyncWithdrawalClientWrapper asyncWithdrawalClientWrapper,
                                        AccountService accountService) {
        this.asyncDepositClientWrapper = asyncDepositClientWrapper;
        this.asyncTransferClientWrapper = asyncTransferClientWrapper;
        this.asyncWithdrawalClientWrapper = asyncWithdrawalClientWrapper;
        this.accountService = accountService;
    }

    public void start() {
        asyncDepositClientWrapper.start();
        asyncTransferClientWrapper.start();
        asyncWithdrawalClientWrapper.start();
    }

    @Override
    public Common.AccountId createAccount() {
        System.out.println("Creating account");
        return accountService.createAccount();
    }

    @Override
    public DepositService.DepositId deposit(DepositService.DepositId depositId, Common.AccountId accountId, Common.CurrencyAmount amount) {
        System.out.println("Deposit request %s".formatted(depositId.value()));
        return asyncDepositClientWrapper.requestDeposit(depositId, accountId, amount);
    }

    @Override
    public DepositService.DepositState getDepositState(DepositService.DepositId id) {
        System.out.println("Getting deposit state %s".formatted(id.value()));
        return asyncDepositClientWrapper.getRequestState(new DepositService.DepositId(id.value()));
    }

    @Override
    public WithdrawalService.WithdrawalId withdraw(WithdrawalService.WithdrawalId withdrawalId, Common.AccountId accountId, WithdrawalService.Address address, Common.CurrencyAmount amount) {
        System.out.println("Withdrawal request %s".formatted(withdrawalId.value()));
        return asyncWithdrawalClientWrapper.requestWithdrawal(withdrawalId, accountId, address, amount);
    }

    @Override
    public Common.WithdrawalState getWithdrawalState(WithdrawalService.WithdrawalId id) {
        System.out.println("Getting withdrawal state %s".formatted(id.value()));
        return asyncWithdrawalClientWrapper.getRequestState(id);
    }

    @Override
    public AccountService.Balances getBalances(Common.AccountId accountId) {
        System.out.println("Getting balances for account %s".formatted(accountId.value()));
        var balances =
                accountService
                        .getBalance(accountId)
                        .balances()
                        .stream()
                        .map(balance -> new AccountService.Balance(balance.currency(), balance.availableBalance(), balance.lockedBalance()))
                        .collect(Collectors.toList());
        return new AccountService.Balances(balances);
    }

    @Override
    public TransferService.TransferId requestTransfer(TransferService.TransferId transferId, Common.AccountId from, Common.AccountId to, Common.CurrencyAmount amount) {
        System.out.println("Transfer request %s".formatted(transferId.value()));
        return asyncTransferClientWrapper.requestTransfer(transferId, from, to, amount);
    }

    @Override
    public TransferService.TransferState getTransferState(TransferService.TransferId id) {
        System.out.println("Getting transfer state %s".formatted(id.value()));
        return asyncTransferClientWrapper.getRequestState(id);
    }
}
