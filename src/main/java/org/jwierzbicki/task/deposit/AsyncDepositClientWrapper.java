package org.jwierzbicki.task.deposit;

import org.jwierzbicki.task.account.AccountService;
import org.jwierzbicki.task.deposit.DepositService.DepositId;
import org.jwierzbicki.task.deposit.DepositService.DepositState;
import org.jwierzbicki.task.Common;

import java.util.concurrent.*;

import static org.jwierzbicki.task.deposit.DepositService.DepositState.FAILED;

public class AsyncDepositClientWrapper {

    private final AccountService accountService;
    private final ConcurrentMap<DepositId, DepositService.Deposit> depositRepository = new ConcurrentHashMap<>();
    private final DepositService depositService;
    private final ScheduledExecutorService executorService;

    public AsyncDepositClientWrapper(AccountService accountService,
                                     DepositService depositService,
                                     ScheduledExecutorService executorService) {
        this.accountService = accountService;
        this.depositService = depositService;
        this.executorService = executorService;
    }

    public void start() {
        scheduleDepositsUpdates();
    }

    public DepositId requestDeposit(DepositId depositId, Common.AccountId accountId, Common.CurrencyAmount amount) {
        if(!accountService.accountExists(accountId)) {
            throw new IllegalArgumentException("Account %s does not exist".formatted(accountId.value()));
        }
        DepositService.Deposit deposit = new DepositService.Deposit(accountId, amount, DepositService.DepositState.PENDING);
        var previous = depositRepository.putIfAbsent(depositId, deposit);
        if (previous != null) {
            throw new IllegalStateException("Deposit request with id[%s] is already present".formatted(depositId.value()));
        }
        depositService.requestDeposit(depositId, accountId, amount);
        return depositId;
    }

    public DepositState getRequestState(DepositId id) {
        if(!depositRepository.containsKey(id)) {
            throw new IllegalArgumentException("Deposit request %s is not found".formatted(id.value()));
        }
        return depositRepository.get(id).depositState();
    }

    private void scheduleDepositsUpdates() {
        executorService.scheduleAtFixedRate(this::updateStates, 0, 1, TimeUnit.SECONDS);
    }

    private void updateStates() {
        depositRepository.forEach((id, deposit) -> {
            if(!isDepositFinished(deposit)) {
                try {
                    DepositState externalDepositState = depositService.getRequestState(id);
                    if (externalDepositState == DepositState.SUCCESS) {
                        finishDeposit(id, DepositService.DepositState.SUCCESS);
                    } else if (externalDepositState == FAILED) {
                        finishDeposit(id, FAILED);
                        System.out.println("Deposit %s failed".formatted(id.value()));
                    }
                } catch (IllegalArgumentException ex) {
                    System.out.println("Deposit %s not found".formatted(id.value()));
                    finishDeposit(id, FAILED);
                } catch (Exception ex) {
                    System.out.println("Error while checking deposit %s".formatted(id.value()));
                }
            }
        });
    }

    //transactional?
    private void finishDeposit(DepositId depositId, DepositState state) {
        System.out.println("Finishing deposit %s".formatted(depositId.value()));
        if(state == DepositService.DepositState.SUCCESS) {
            var depo = depositRepository.get(depositId);
            if(depo != null) {
                accountService.addFunds(depo.accountId(), depo.amount());
                depositRepository.put(depositId, depo.copyWithState(DepositState.SUCCESS));
                System.out.println("Deposit %s finished".formatted(depositId.value()));
            } else {
                // log error
                // leave deposit in pending state to check it later
            }
        } else if(state == FAILED) {
            depositRepository.computeIfPresent(depositId, (id, deposit) -> deposit.copyWithState(FAILED));
            System.out.println("Deposit %s failed".formatted(depositId.value()));
        } else {
            // unexpected state
            // log error
        }
    }

    private static boolean isDepositFinished(DepositService.Deposit deposit) {
        return deposit.depositState() == DepositService.DepositState.SUCCESS || deposit.depositState() == FAILED;
    }

}
