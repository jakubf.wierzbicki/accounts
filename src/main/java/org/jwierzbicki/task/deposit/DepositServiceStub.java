package org.jwierzbicki.task.deposit;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.random.RandomGenerator;

import static org.jwierzbicki.task.deposit.DepositService.DepositState.*;

import org.jwierzbicki.task.Common;
import org.jwierzbicki.task.Common.AccountId;


public class DepositServiceStub implements DepositService {

    private final ConcurrentMap<DepositId, Deposit> requests = new ConcurrentHashMap<>();

    @Override
    public void requestDeposit(DepositId depositId, AccountId to, Common.CurrencyAmount amount) {
        final var existing = requests.putIfAbsent(depositId, new Deposit(finalState(), finaliseAt(), to, amount));
        if (existing != null)
            throw new IllegalStateException("Deposit request with id[%s] is already present".formatted(depositId));
    }

    private DepositState finalState() {
        return RandomGenerator.getDefault().nextInt(100) > 5  ? SUCCESS : FAILED;
    }

    private long finaliseAt() {
        return System.currentTimeMillis() + ThreadLocalRandom.current().nextLong(100, 1000);
    }


    @Override
    public DepositState getRequestState(DepositId id) {
        final var request = requests.get(id);
        if (request == null)
            throw new IllegalArgumentException("Request %s is not found".formatted(id));
        return request.finalState();
    }

    record Deposit(DepositState state, long finaliseAt, AccountId to, Common.CurrencyAmount amount) {
        public DepositState finalState() {
            return finaliseAt <= System.currentTimeMillis() ? state : DepositState.PROCESSING;
        }
    }
}

