package org.jwierzbicki.task.deposit;

import org.jwierzbicki.task.Common;


public interface DepositService {

    void requestDeposit(DepositId depositId, Common.AccountId to, Common.CurrencyAmount currencyAmount);

    DepositState getRequestState(DepositId id);

    enum DepositState {
        PENDING, PROCESSING, FAILED, SUCCESS
    }

    record DepositId(String value) {}

    record Deposit(Common.AccountId accountId, Common.CurrencyAmount amount, DepositState depositState) {
        public Deposit copyWithState(DepositState state) {
            return new Deposit(accountId, amount, state);
        }
    }
}
