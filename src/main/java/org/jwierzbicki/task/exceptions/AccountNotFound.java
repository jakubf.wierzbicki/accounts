package org.jwierzbicki.task.exceptions;

public class AccountNotFound extends RuntimeException {
    public AccountNotFound(String accountId) {
        super("Account with id " + accountId + " not found");
    }
}
