package org.jwierzbicki.task.exceptions;

public class AccountAlreadyExists extends RuntimeException {

    public AccountAlreadyExists(String accountId) {
        super("Account with id " + accountId + " already exists");
    }

}
