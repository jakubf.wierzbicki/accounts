package org.jwierzbicki.task;


import io.grpc.Server;
import io.grpc.ServerBuilder;
import org.jwierzbicki.task.account.AccountService;
import org.jwierzbicki.task.account.AccountServiceImpl;
import org.jwierzbicki.task.deposit.AsyncDepositClientWrapper;
import org.jwierzbicki.task.deposit.DepositService;
import org.jwierzbicki.task.deposit.DepositServiceStub;
import org.jwierzbicki.task.management.*;
import org.jwierzbicki.task.server.AccountServer;
import org.jwierzbicki.task.transfer.AsyncTransferClientWrapper;
import org.jwierzbicki.task.transfer.TransferService;
import org.jwierzbicki.task.transfer.TransferServiceStub;
import org.jwierzbicki.task.withdrawal.AsyncWithdrawalClientWrapper;
import org.jwierzbicki.task.withdrawal.WithdrawalService;
import org.jwierzbicki.task.withdrawal.WithdrawalServiceStub;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class Main {

    public static void main(String[] args) {
        ExecutorService forkJoinPool = Executors.newFixedThreadPool(100);

        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(10);
        AccountService accountService = new AccountServiceImpl();
        WithdrawalService withdrawalService = new WithdrawalServiceStub();
        AccountManagementService accountManagementService = getAccountManagementService(accountService, withdrawalService, executorService);
        accountManagementService.start();
        Server grpcServer = ServerBuilder.forPort(8999).addService(new AccountServer(accountManagementService, forkJoinPool)).build();
        try {
            grpcServer.start();
            System.out.println("Server started");
            grpcServer.awaitTermination();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static AccountManagementService getAccountManagementService(AccountService accountService, WithdrawalService withdrawalService, ScheduledExecutorService executorService) {
        DepositService depositService = new DepositServiceStub();
        TransferService transferService = new TransferServiceStub();
        AsyncWithdrawalClientWrapper asyncWithdrawalClientWrapper = new AsyncWithdrawalClientWrapper(accountService, withdrawalService, executorService);
        AsyncTransferClientWrapper asyncTransferClientWrapper = new AsyncTransferClientWrapper(accountService, transferService, executorService);
        AsyncDepositClientWrapper asyncDepositClientWrapper = new AsyncDepositClientWrapper(accountService, depositService, executorService);
        AccountManagementService accountManagementService = new AccountManagementServiceImpl(asyncDepositClientWrapper, asyncTransferClientWrapper, asyncWithdrawalClientWrapper, accountService);
        return accountManagementService;
    }

}
