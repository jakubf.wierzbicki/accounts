package org.jwierzbicki.task.server;

import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.protobuf.ProtoUtils;
import io.grpc.stub.StreamObserver;
import org.jwierzbicki.protobuf.Transfers;
import org.jwierzbicki.task.deposit.DepositService;
import org.jwierzbicki.task.management.AccountManagementService;
import org.jwierzbicki.task.Common;
import org.jwierzbicki.task.transfer.TransferService;
import org.jwierzbicki.task.withdrawal.WithdrawalService;

import java.util.UUID;
import java.util.concurrent.ExecutorService;

public class AccountServer extends org.jwierzbicki.protobuf.AccountServiceGrpc.AccountServiceImplBase {

    private static final Metadata.Key<Transfers.ErrorResponse> ERROR_RESPONSE_KEY = ProtoUtils.keyForProto(Transfers.ErrorResponse.getDefaultInstance());

    private final AccountManagementService accountManagementService;
    private final ExecutorService executorService;

    public AccountServer(AccountManagementService accountManagementService,
                         ExecutorService executorService) {
        this.accountManagementService = accountManagementService;
        this.executorService = executorService;
    }

    @Override
    public void createAccount(Transfers.CreateAccountRequest request, StreamObserver<Transfers.CreateAccountResponse> responseObserver) {
        executorService.submit(() -> {
                    try {
                        var accId = accountManagementService.createAccount();
                        responseObserver.onNext(Transfers.CreateAccountResponse.newBuilder().setAccount(Transfers.Account.newBuilder().setId(accId.value().toString()).build()).build());
                        responseObserver.onCompleted();
                    } catch (Exception e) {
                        errorHandler(responseObserver, e);
                    }
                });
    }

    @Override
    public void deposit(Transfers.DepositRequest request, StreamObserver<Transfers.DepositStatusResponse> responseObserver) {
        executorService.submit(() -> {
            try {
                var depositId = new DepositService.DepositId(request.getId());
                var accountId = new Common.AccountId(request.getAccount().getId());
                var amount = new Common.CurrencyAmount(Mapping.convertBigDecimal(request.getAmount().getAmount()), request.getAmount().getCurrency());
                accountManagementService.deposit(depositId, accountId, amount);
                var depositState = accountManagementService.getDepositState(depositId);
                responseObserver.onNext(Transfers.DepositStatusResponse.newBuilder().setState(convertDepositState(depositState)).build());
                responseObserver.onCompleted();
            } catch (Exception e) {
                errorHandler(responseObserver, e);
            }
        });
    }


    @Override
    public void depositStatus(Transfers.DepositStatusRequest request, StreamObserver<Transfers.DepositStatusResponse> responseObserver) {
        executorService.submit(() -> {
            try {
                var depositId = new DepositService.DepositId(request.getId());
                var depositState = accountManagementService.getDepositState(depositId);
                responseObserver.onNext(Transfers.DepositStatusResponse.newBuilder().setState(convertDepositState(depositState)).build());
                responseObserver.onCompleted();
            } catch (Exception e) {
                errorHandler(responseObserver, e);
            }
        });
    }

    @Override
    public void withdraw(Transfers.WithdrawRequest request, StreamObserver<Transfers.WithdrawStatusResponse> responseObserver) {
        executorService.submit(() -> {
            try {
                var withdrawalId = new WithdrawalService.WithdrawalId(UUID.fromString(request.getId()));
                var accountId = new Common.AccountId(request.getAccount().getId());
                var address = new WithdrawalService.Address(request.getAddress());
                var amount = new Common.CurrencyAmount(Mapping.convertBigDecimal(request.getAmount().getAmount()), request.getAmount().getCurrency());
                accountManagementService.withdraw(withdrawalId, accountId, address, amount);
                var withdrawalState = accountManagementService.getWithdrawalState(withdrawalId);
                responseObserver.onNext(Transfers.WithdrawStatusResponse.newBuilder().setState(convertWithdrawState(withdrawalState)).build());
                responseObserver.onCompleted();
            } catch (Exception e) {
                errorHandler(responseObserver, e);
            }
        });
    }

    @Override
    public void withdrawStatus(Transfers.WithdrawStatusRequest request, StreamObserver<Transfers.WithdrawStatusResponse> responseObserver) {
        executorService.submit(() -> {
            try {
                var withdrawalId = new WithdrawalService.WithdrawalId(UUID.fromString(request.getId()));
                var withdrawalState = accountManagementService.getWithdrawalState(withdrawalId);
                responseObserver.onNext(Transfers.WithdrawStatusResponse.newBuilder().setState(convertWithdrawState(withdrawalState)).build());
                responseObserver.onCompleted();
            } catch (Exception e) {
                errorHandler(responseObserver, e);
            }
        });
    }

    @Override
    public void transfer(Transfers.TransferRequest request, StreamObserver<Transfers.TransferStatusResponse> responseObserver) {
        executorService.submit(() -> {
            try {
                var transferId = new TransferService.TransferId(UUID.fromString(request.getId()));
                var from = new Common.AccountId(request.getFrom().getId());
                var to = new Common.AccountId(request.getTo().getId());
                var amount = new Common.CurrencyAmount(Mapping.convertBigDecimal(request.getAmount().getAmount()), request.getAmount().getCurrency());
                accountManagementService.requestTransfer(transferId, from, to, amount);
                var transferState = accountManagementService.getTransferState(transferId);
                responseObserver.onNext(Transfers.TransferStatusResponse.newBuilder().setState(convertTransferState(transferState)).build());
                responseObserver.onCompleted();
            } catch (Exception e) {
                errorHandler(responseObserver, e);
            }
        });
    }

    @Override
    public void transferStatus(Transfers.TransferStatusRequest request, StreamObserver<Transfers.TransferStatusResponse> responseObserver) {
        executorService.submit(() -> {
            try {
                var transferId = new TransferService.TransferId(UUID.fromString(request.getId()));
                var transferState = accountManagementService.getTransferState(transferId);
                responseObserver.onNext(Transfers.TransferStatusResponse.newBuilder().setState(convertTransferState(transferState)).build());
                responseObserver.onCompleted();
            } catch (Exception e) {
                errorHandler(responseObserver, e);
            }
        });
    }

    @Override
    public void balances(Transfers.BalancesRequest request, StreamObserver<Transfers.BalancesResponse> responseObserver) {
        executorService.submit(() -> {

            try {
                var accountId = new Common.AccountId(request.getAccount().getId());
                var balances = accountManagementService.getBalances(accountId);
                var response = Transfers.BalancesResponse.newBuilder();
                balances.balances().forEach(balance -> response.addBalances(Transfers.Balance.newBuilder()
                        .setCurrency(balance.currency())
                        .setAvailableAmount(Mapping.convertBigDecimal(balance.availableBalance()))
                        .setLockedAmount(Mapping.convertBigDecimal(balance.lockedBalance()))
                        .build()));
                responseObserver.onNext(response.build());
                responseObserver.onCompleted();
            } catch (Exception e) {
                errorHandler(responseObserver, e);
            }
        });
    }

    private void errorHandler(StreamObserver<?> responseObserver, Throwable e) {
        System.err.println("Error occurred: " + e.getMessage());
        var errorResponse = Transfers.ErrorResponse.newBuilder().setMessage(e.getMessage()).build();
        Metadata metadata = new Metadata();
        metadata.put(ERROR_RESPONSE_KEY, errorResponse);
        responseObserver.onError(Status.INTERNAL.asRuntimeException(metadata));
    }


    private Transfers.DepositState convertDepositState(DepositService.DepositState depositState) {
        return switch (depositState) {
            case PENDING -> Transfers.DepositState.DS_PENDING;
            case PROCESSING -> Transfers.DepositState.DS_PROCESSING;
            case FAILED -> Transfers.DepositState.DS_REJECTED;
            case SUCCESS -> Transfers.DepositState.DS_SUCCESS;
            default -> throw new IllegalArgumentException("Unknown deposit state: " + depositState);
        };
    }

    private Transfers.WithdrawState convertWithdrawState(Common.WithdrawalState withdrawalState) {
        return switch (withdrawalState) {
            case PENDING -> Transfers.WithdrawState.WS_PENDING;
            case PROCESSING -> Transfers.WithdrawState.WS_PROCESSING;
            case FAILED -> Transfers.WithdrawState.WS_REJECTED;
            case SUCCESS -> Transfers.WithdrawState.WS_SUCCESS;
            default -> throw new IllegalArgumentException("Unknown withdrawal state: " + withdrawalState);
        };
    }

    private Transfers.TransferState convertTransferState(TransferService.TransferState transferState) {
        return switch (transferState) {
            case PENDING -> Transfers.TransferState.TS_PENDING;
            case PROCESSING -> Transfers.TransferState.TS_PROCESSING;
            case SUCCESS -> Transfers.TransferState.TS_SUCCESS;
            case FAILURE -> Transfers.TransferState.TS_REJECTED;
            default -> throw new IllegalArgumentException("Unknown transfer state: " + transferState);
        };
    }

}
