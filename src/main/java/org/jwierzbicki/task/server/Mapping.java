package org.jwierzbicki.task.server;

import com.google.protobuf.ByteString;
import org.jwierzbicki.protobuf.Transfers;

import java.math.BigDecimal;
import java.math.BigInteger;

public class Mapping {
    public static BigDecimal convertBigDecimal(Transfers.BDecimal amount) {
        return new BigDecimal(new BigInteger(amount.getIntVal().getValue().toByteArray()), amount.getScale());
    }

    public static Transfers.BDecimal convertBigDecimal(BigDecimal amount) {
        return Transfers.BDecimal.newBuilder().setIntVal(Transfers.BInteger.newBuilder().setValue(ByteString.copyFrom(amount.unscaledValue().toByteArray())).build()).setScale(amount.scale()).build();
    }
}
