package org.jwierzbicki.task;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.jwierzbicki.protobuf.AccountServiceGrpc;
import org.jwierzbicki.protobuf.Transfers;
import org.jwierzbicki.task.server.Mapping;

import java.math.BigDecimal;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

import static java.util.concurrent.locks.LockSupport.parkNanos;

public class Example {

    public static void main(String[] args) throws InterruptedException {
        for(int i = 0; i < 2; i++) {
            new Thread(() -> {
                try {
                    exampleUsage();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }).start();
        }
    }

    private static void exampleUsage() throws InterruptedException {
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 8999).usePlaintext().build();
        AccountServiceGrpc.AccountServiceBlockingStub bookStub = AccountServiceGrpc.newBlockingStub(channel);
        log("Hello from client");
        long initBalanceLong = ThreadLocalRandom.current().nextLong(200, 1000);
        long withdrawBalanceLong = ThreadLocalRandom.current().nextLong(100, initBalanceLong);
        long transferBalanceLong = ThreadLocalRandom.current().nextLong(1, initBalanceLong - withdrawBalanceLong);
        var initBalance = BigDecimal.valueOf(initBalanceLong);
        var withdrawAmount = BigDecimal.valueOf(withdrawBalanceLong);
        var transferAmount = BigDecimal.valueOf(transferBalanceLong);
        var response = bookStub.createAccount(Transfers.CreateAccountRequest.newBuilder().build());
        var mainAcc = response.getAccount();
        log("Response: " + mainAcc.getId());
        depositSomething(bookStub, response, initBalance);
        log("Deposit success. Check balances..");
        checkBalances(bookStub, mainAcc, initBalance);
        log("Withdraw some money..");
        withdrawDollar(bookStub, mainAcc, withdrawAmount);
        log("Withdraw success. Check balances..");
        checkBalances(bookStub, mainAcc, initBalance.subtract(withdrawAmount));
        log("Create another account..");
        response = bookStub.createAccount(Transfers.CreateAccountRequest.newBuilder().build());
        var anotherAcc = response.getAccount();
        log("Response: " + anotherAcc.getId());
        log("let's transfer some money..");
        doTransfer(mainAcc, anotherAcc, bookStub, transferAmount);
        log("Transfer success. Check balances..");
        checkBalances(bookStub, mainAcc, initBalance.subtract(withdrawAmount).subtract(transferAmount));
        checkBalances(bookStub, anotherAcc, transferAmount);
        channel.shutdown();
    }
    
    private static void log(String message) {
        System.out.println("[%s], %s".formatted(Thread.currentThread().getId(), message));
    }

    private static void logError(String message) {
        System.err.println("[%s], %s".formatted(Thread.currentThread().getId(), message));
    }

    private static void doTransfer(Transfers.Account mainAcc, Transfers.Account anotherAcc, AccountServiceGrpc.AccountServiceBlockingStub bookStub, BigDecimal amount) {
        var transferRequest = createTransferRequest(mainAcc, anotherAcc, amount);
        var transferStatus = bookStub.transfer(transferRequest).getState();
        while (transferStatus != Transfers.TransferState.TS_SUCCESS) {
            if (transferStatus == Transfers.TransferState.TS_PENDING) {
                transferStatus = bookStub.transferStatus(Transfers.TransferStatusRequest.newBuilder().setId(transferRequest.getId()).build()).getState();
                parkNanos(1000000000);
            } else if (transferStatus == Transfers.TransferState.TS_REJECTED) {
                log("Transfer failed. Trying again..");
                transferRequest = createTransferRequest(mainAcc, anotherAcc, amount);
                transferStatus = bookStub.transfer(transferRequest).getState();
                log("Transfer initiated: %s".formatted(transferRequest.getId()));
            }
        }
    }

    private static Transfers.TransferRequest createTransferRequest(Transfers.Account from, Transfers.Account to, BigDecimal amount) {
        return Transfers.TransferRequest.newBuilder()
                .setId(UUID.randomUUID().toString())
                .setFrom(from)
                .setTo(to)
                .setAmount(Transfers.CurrencyAmount.newBuilder().setAmount(Mapping.convertBigDecimal(amount)).setCurrency("USD").build())
                .build();
    }

    private static void withdrawDollar(AccountServiceGrpc.AccountServiceBlockingStub bookStub, Transfers.Account account, BigDecimal amount) {
        Transfers.WithdrawRequest withdrawRequest = createWithdrawRequest(account, amount);
        var withdrawStatus = bookStub.withdraw(withdrawRequest).getState();
        while(withdrawStatus != Transfers.WithdrawState.WS_SUCCESS) {
            if (withdrawStatus == Transfers.WithdrawState.WS_PENDING) {
                withdrawStatus = requestWithdrawStatus(bookStub, withdrawRequest);
                parkNanos(1000000000);
            } else if (withdrawStatus == Transfers.WithdrawState.WS_REJECTED) {
                log("Withdraw failed. Trying again..");
                withdrawRequest = createWithdrawRequest(account, amount);
                withdrawStatus = bookStub.withdraw(withdrawRequest).getState();
                log("Withdraw initiated: %s".formatted(withdrawRequest.getId()));
            }
        }
    }

    private static Transfers.WithdrawState requestWithdrawStatus(AccountServiceGrpc.AccountServiceBlockingStub bookStub, Transfers.WithdrawRequest withdrawRequest) {
        return bookStub.withdrawStatus(Transfers.WithdrawStatusRequest.newBuilder().setId(withdrawRequest.getId()).build()).getState();
    }

    private static Transfers.WithdrawRequest createWithdrawRequest(Transfers.Account account, BigDecimal amount) {
        return Transfers.WithdrawRequest
                .newBuilder()
                .setAddress(UUID.randomUUID().toString())
                .setId(UUID.randomUUID().toString())
                .setAccount(account)
                .setAmount(Transfers.CurrencyAmount
                        .newBuilder()
                        .setAmount(Mapping.convertBigDecimal(amount))
                        .setCurrency("USD")
                        .build())
                .build();
    }

    private static void checkBalances(AccountServiceGrpc.AccountServiceBlockingStub bookStub, Transfers.Account account, BigDecimal expectedBalance) {
        var balances = bookStub.balances(Transfers.BalancesRequest.newBuilder().setAccount(account).build());
        if(balances.getBalancesCount() == 0 || Mapping.convertBigDecimal(balances.getBalances(0).getAvailableAmount()).compareTo(expectedBalance) != 0) {
            logError("Balance is not as expected. Expected: %s, actual: %s".formatted(expectedBalance, balances.getBalances(0).getAvailableAmount()));
            System.exit(1);
        }

    }

    private static void depositSomething(AccountServiceGrpc.AccountServiceBlockingStub bookStub, Transfers.CreateAccountResponse response, BigDecimal amount)  {
        var depositRequest = createDepositRequest(response, amount);
        var depositStatus = requestDeposit(bookStub, depositRequest);
        while (depositStatus != Transfers.DepositState.DS_SUCCESS) {
            if(depositStatus == Transfers.DepositState.DS_PENDING) {
                depositStatus = requestDepositState(bookStub, depositRequest);
                parkNanos(1000000000);
            } else if (depositStatus == Transfers.DepositState.DS_REJECTED) {
                log("Deposit failed. Trying again..");
                depositRequest = createDepositRequest(response, amount);
                depositStatus = requestDeposit(bookStub, depositRequest);
                log("Deposit initiated: " + depositRequest.getId());
            }
        }
    }

    private static Transfers.DepositRequest createDepositRequest(Transfers.CreateAccountResponse response, BigDecimal amount) {
        return Transfers.DepositRequest.newBuilder()
                .setId(UUID.randomUUID().toString())
                .setAccount(response.getAccount())
                .setAmount(Transfers.CurrencyAmount.newBuilder().setAmount(Mapping.convertBigDecimal(amount)).setCurrency("USD").build())
                .build();
    }

    private static Transfers.DepositState requestDepositState(AccountServiceGrpc.AccountServiceBlockingStub bookStub, Transfers.DepositRequest depositRequest) {
        return bookStub.depositStatus(Transfers.DepositStatusRequest.newBuilder().setId(depositRequest.getId()).build()).getState();
    }

    private static Transfers.DepositState requestDeposit(AccountServiceGrpc.AccountServiceBlockingStub bookStub, Transfers.DepositRequest depositRequest) {
        return bookStub.deposit(depositRequest).getState();
    }

}
