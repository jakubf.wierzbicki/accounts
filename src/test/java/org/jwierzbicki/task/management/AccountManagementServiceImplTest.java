package org.jwierzbicki.task.management;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.jwierzbicki.task.Common;
import org.jwierzbicki.task.account.AccountService;
import org.jwierzbicki.task.deposit.AsyncDepositClientWrapper;
import org.jwierzbicki.task.deposit.DepositService;
import org.jwierzbicki.task.transfer.AsyncTransferClientWrapper;
import org.jwierzbicki.task.withdrawal.AsyncWithdrawalClientWrapper;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.UUID;

import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AccountManagementServiceImplTest {

    @Mock
    private AsyncDepositClientWrapper asyncDepositClientWrapper;
    @Mock
    private AsyncTransferClientWrapper asyncTransferClientWrapper;
    @Mock
    private AsyncWithdrawalClientWrapper asyncWithdrawalClientWrapper;
    @Mock
    private AccountService accountService;

    private AccountManagementServiceImpl accountManagementService;


    @Before
    public void setUp() {
        reset(asyncDepositClientWrapper, asyncTransferClientWrapper, asyncWithdrawalClientWrapper, accountService);
        accountManagementService = new AccountManagementServiceImpl(asyncDepositClientWrapper, asyncTransferClientWrapper, asyncWithdrawalClientWrapper, accountService);
    }


    @Test
    public void should_create_account() {
        when(accountService.createAccount()).thenReturn(new Common.AccountId(UUID.randomUUID().toString()));
        var result = accountManagementService.createAccount();
        Assert.assertNotNull(result);
    }

    @Test
    public void should_not_create_account_if_exception() {
        when(accountService.createAccount()).thenThrow(new RuntimeException());
        assertThrows(RuntimeException.class, accountManagementService::createAccount);
    }

    @Test
    public void should_return_empty_balances_for_new_account() {
        var accountId = new Common.AccountId(UUID.randomUUID().toString());
        when(accountService.getBalance(accountId)).thenReturn(new AccountService.Balances(Collections.emptyList()));
        var result = accountManagementService.getBalances(accountId);
        Assert.assertNotNull(result);
        Assert.assertTrue(result.balances().isEmpty());
    }

    @Test
    public void should_not_return_balances_if_exception() {
        var accountId = new Common.AccountId(UUID.randomUUID().toString());
        when(accountService.getBalance(accountId)).thenThrow(new RuntimeException());
        assertThrows(RuntimeException.class, () -> accountManagementService.getBalances(accountId));
    }

    @Test
    public void should_deposit() {
        var accountId = new Common.AccountId(UUID.randomUUID().toString());
        var amount = new Common.CurrencyAmount(java.math.BigDecimal.ONE, "USD");
        var depositId = new DepositService.DepositId(UUID.randomUUID().toString());

        when(asyncDepositClientWrapper.requestDeposit(depositId, accountId, amount)).thenReturn(new DepositService.DepositId(UUID.randomUUID().toString()));
        var result = accountManagementService.deposit(depositId, accountId, amount);
        Assert.assertNotNull(result);
    }

    //and so on



}