package org.jwierzbicki.task.transfer;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.jwierzbicki.task.account.AccountService;
import org.jwierzbicki.task.Common;
import org.jwierzbicki.task.transfer.TransferService.TransferState;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.concurrent.ScheduledExecutorService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AsyncTransferClientWrapperTest {

    @Mock
    private ScheduledExecutorService executorService;

    @Mock
    private AccountService accountService;

    @Mock
    private TransferService transferService;

    private AsyncTransferClientWrapper asyncTransferClientWrapper;

    @Before
    public void setUp() {
        asyncTransferClientWrapper = new AsyncTransferClientWrapper(accountService, transferService, executorService);

    }

    @Test
    public void should_lock_balance_and_request_transfer_if_accounts_exist() {
        // Given
        var amount = new Common.CurrencyAmount(java.math.BigDecimal.ONE, "USD");
        var fromAccountId = new Common.AccountId(java.util.UUID.randomUUID().toString());
        var toAccountId = new Common.AccountId(java.util.UUID.randomUUID().toString());
        var transferId = new TransferService.TransferId(java.util.UUID.randomUUID());

        // When
        when(accountService.accountExists(fromAccountId)).thenReturn(true);
        when(accountService.accountExists(toAccountId)).thenReturn(true);

        // Then
        asyncTransferClientWrapper.requestTransfer(transferId, fromAccountId, toAccountId, amount);
        verify(accountService).lockBalance(fromAccountId, amount);
        verify(transferService).requestTransfer(any(), eq(fromAccountId), eq(toAccountId), eq(amount));
    }

    @Test
    public void should_not_request_transfer_if_to_account_does_not_exist() {
        // Given
        var fromAccountId = new Common.AccountId(java.util.UUID.randomUUID().toString());
        var toAccountId = new Common.AccountId(java.util.UUID.randomUUID().toString());
        var amount = new Common.CurrencyAmount(java.math.BigDecimal.ONE, "USD");
        var transferId = new TransferService.TransferId(java.util.UUID.randomUUID());

        when(accountService.accountExists(fromAccountId)).thenReturn(true);
        when(accountService.accountExists(toAccountId)).thenReturn(false);
        assertThrows(IllegalArgumentException.class, () -> asyncTransferClientWrapper.requestTransfer(transferId, fromAccountId, toAccountId, amount));
        verifyNoInteractions(transferService);
        verify(accountService, never()).lockBalance(any(), any());
    }

    @Test
    public void should_not_request_transfer_if_from_account_does_not_exist() {
        // Given
        var fromAccountId = new Common.AccountId(java.util.UUID.randomUUID().toString());
        var toAccountId = new Common.AccountId(java.util.UUID.randomUUID().toString());
        var amount = new Common.CurrencyAmount(java.math.BigDecimal.ONE, "USD");
        var transferId = new TransferService.TransferId(java.util.UUID.randomUUID());

        when(accountService.accountExists(fromAccountId)).thenReturn(false);
        assertThrows(IllegalArgumentException.class, () -> asyncTransferClientWrapper.requestTransfer(transferId, fromAccountId, toAccountId, amount));
        verifyNoInteractions(transferService);
        verify(accountService, never()).lockBalance(any(), any());
    }

    @Test
    public void should_lock_funds_if_request_sent() {
        // Given
        var fromAccountId = new Common.AccountId(java.util.UUID.randomUUID().toString());
        var toAccountId = new Common.AccountId(java.util.UUID.randomUUID().toString());
        var amount = new Common.CurrencyAmount(java.math.BigDecimal.ONE, "USD");
        var transferId = new TransferService.TransferId(java.util.UUID.randomUUID());

        // When
        when(accountService.accountExists(fromAccountId)).thenReturn(true);
        when(accountService.accountExists(toAccountId)).thenReturn(true);

        // Then
        asyncTransferClientWrapper.requestTransfer(transferId, fromAccountId, toAccountId, amount);
        verify(accountService).lockBalance(fromAccountId, amount);
    }

    @Test
    public void should_unlock_funds_if_request_failed() {
        // Given
        var fromAccountId = new Common.AccountId(java.util.UUID.randomUUID().toString());
        var toAccountId = new Common.AccountId(java.util.UUID.randomUUID().toString());
        var amount = new Common.CurrencyAmount(java.math.BigDecimal.ONE, "USD");
        var transferId = new TransferService.TransferId(java.util.UUID.randomUUID());

        // Simulate a failed transfer
        when(transferService.getRequestState(any())).thenReturn(TransferState.FAILURE);
        when(accountService.accountExists(fromAccountId)).thenReturn(true);
        when(accountService.accountExists(toAccountId)).thenReturn(true);

        asyncTransferClientWrapper.requestTransfer(transferId, fromAccountId, toAccountId, amount);
        verify(accountService).lockBalance(fromAccountId, amount);
        asyncTransferClientWrapper.start();
        ArgumentCaptor<Runnable> captor = ArgumentCaptor.forClass(Runnable.class);
        verify(executorService).scheduleAtFixedRate(captor.capture(), anyLong(), anyLong(), any());
        captor.getValue().run(); // Manually trigger the update check

        verify(accountService).unlockBalance(fromAccountId, amount);
    }


    @Test
    public void should_transfer_locked_funds_on_success() {
        var fromAccountId = new Common.AccountId(java.util.UUID.randomUUID().toString());
        var toAccountId = new Common.AccountId(java.util.UUID.randomUUID().toString());
        var amount = new Common.CurrencyAmount(java.math.BigDecimal.ONE, "USD");
        var transferId = new TransferService.TransferId(java.util.UUID.randomUUID());

        // Simulate a successful transfer
        when(transferService.getRequestState(any())).thenReturn(TransferState.SUCCESS);
        when(accountService.accountExists(fromAccountId)).thenReturn(true);
        when(accountService.accountExists(toAccountId)).thenReturn(true);

        asyncTransferClientWrapper.requestTransfer(transferId, fromAccountId, toAccountId, amount);
        asyncTransferClientWrapper.start();
        ArgumentCaptor<Runnable> captor = ArgumentCaptor.forClass(Runnable.class);
        verify(executorService).scheduleAtFixedRate(captor.capture(), anyLong(), anyLong(), any());
        captor.getValue().run(); // Manually trigger the update check

        verify(accountService).transferLockedFunds(fromAccountId, toAccountId, amount);
    }

    @Test
    public void should_not_transfer_locked_funds_on_failure() {
        var fromAccountId = new Common.AccountId(java.util.UUID.randomUUID().toString());
        var toAccountId = new Common.AccountId(java.util.UUID.randomUUID().toString());
        var amount = new Common.CurrencyAmount(java.math.BigDecimal.ONE, "USD");
        var transferId = new TransferService.TransferId(java.util.UUID.randomUUID());

        // Simulate a failed transfer
        when(transferService.getRequestState(any())).thenReturn(TransferState.FAILURE);
        when(accountService.accountExists(fromAccountId)).thenReturn(true);
        when(accountService.accountExists(toAccountId)).thenReturn(true);

        asyncTransferClientWrapper.requestTransfer(transferId, fromAccountId, toAccountId, amount);
        asyncTransferClientWrapper.start();
        ArgumentCaptor<Runnable> captor = ArgumentCaptor.forClass(Runnable.class);
        verify(executorService).scheduleAtFixedRate(captor.capture(), anyLong(), anyLong(), any());
        captor.getValue().run(); // Manually trigger the update check

        verify(accountService, never()).transferLockedFunds(fromAccountId, toAccountId, amount);
    }

    @Test
    public void should_return_pending_state_if_could_not_transfer_funds() {
        var fromAccountId = new Common.AccountId(java.util.UUID.randomUUID().toString());
        var toAccountId = new Common.AccountId(java.util.UUID.randomUUID().toString());
        var amount = new Common.CurrencyAmount(java.math.BigDecimal.ONE, "USD");
        var transferId = new TransferService.TransferId(java.util.UUID.randomUUID());

        when(transferService.getRequestState(any())).thenReturn(TransferState.SUCCESS);
        when(accountService.accountExists(fromAccountId)).thenReturn(true);
        when(accountService.accountExists(toAccountId)).thenReturn(true);

        asyncTransferClientWrapper.requestTransfer(transferId, fromAccountId, toAccountId, amount);
        asyncTransferClientWrapper.start();
        ArgumentCaptor<Runnable> captor = ArgumentCaptor.forClass(Runnable.class);
        verify(executorService).scheduleAtFixedRate(captor.capture(), anyLong(), anyLong(), any());
        doThrow(new IllegalStateException()).when(accountService).transferLockedFunds(any(), any(), any());
        captor.getValue().run();
        var transferState = asyncTransferClientWrapper.getRequestState(transferId);
        assertEquals(TransferState.PENDING, transferState);
    }


    @Test
    public void should_return_pending_state_if_could_not_unlock_balance() {
        var fromAccountId = new Common.AccountId(java.util.UUID.randomUUID().toString());
        var toAccountId = new Common.AccountId(java.util.UUID.randomUUID().toString());
        var amount = new Common.CurrencyAmount(java.math.BigDecimal.ONE, "USD");
        var transferId = new TransferService.TransferId(java.util.UUID.randomUUID());

        when(transferService.getRequestState(any())).thenReturn(TransferState.FAILURE);
        when(accountService.accountExists(fromAccountId)).thenReturn(true);
        when(accountService.accountExists(toAccountId)).thenReturn(true);

        asyncTransferClientWrapper.requestTransfer(transferId, fromAccountId, toAccountId, amount);
        asyncTransferClientWrapper.start();
        ArgumentCaptor<Runnable> captor = ArgumentCaptor.forClass(Runnable.class);
        verify(executorService).scheduleAtFixedRate(captor.capture(), anyLong(), anyLong(), any());
        doThrow(new IllegalStateException()).when(accountService).unlockBalance(any(), any());
        captor.getValue().run();
        var transferState = asyncTransferClientWrapper.getRequestState(transferId);
        assertEquals(TransferState.PENDING, transferState);
    }



}