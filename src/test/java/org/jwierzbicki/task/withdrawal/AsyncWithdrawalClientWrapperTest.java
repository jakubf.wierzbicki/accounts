package org.jwierzbicki.task.withdrawal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.jwierzbicki.task.account.AccountService;
import org.jwierzbicki.task.Common;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.concurrent.ScheduledExecutorService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AsyncWithdrawalClientWrapperTest {

    @Mock
    private ScheduledExecutorService executorService;

    @Mock
    private AccountService accountService;

    @Mock
    private WithdrawalService withdrawalService;

    private AsyncWithdrawalClientWrapper asyncWithdrawalClientWrapper;

    @Before
    public void setUp() {
        asyncWithdrawalClientWrapper = new AsyncWithdrawalClientWrapper(accountService, withdrawalService, executorService);
    }

    @Test
    public void should_lock_balance_and_request_withdrawal_if_account_has_sufficient_funds() {
        var accountId = new Common.AccountId(java.util.UUID.randomUUID().toString());
        var address = new WithdrawalService.Address("testAddress");
        var amount = new Common.CurrencyAmount(java.math.BigDecimal.ONE, "USD");
        var withdrawalId = new WithdrawalService.WithdrawalId(java.util.UUID.randomUUID());

        when(accountService.accountExists(accountId)).thenReturn(true);
        asyncWithdrawalClientWrapper.requestWithdrawal(withdrawalId, accountId, address, amount);

        verify(accountService).lockBalance(accountId, amount);
        verify(withdrawalService).requestWithdrawal(eq(withdrawalId), eq(address), eq(amount));
    }

    @Test
    public void should_throw_exception_if_account_does_not_exist() {
        var accountId = new Common.AccountId(java.util.UUID.randomUUID().toString());
        var address = new WithdrawalService.Address("testAddress");
        var amount = new Common.CurrencyAmount(java.math.BigDecimal.ONE, "USD");
        var withdrawalId = new WithdrawalService.WithdrawalId(java.util.UUID.randomUUID());

        when(accountService.accountExists(accountId)).thenReturn(false);

        assertThrows(IllegalArgumentException.class, () -> asyncWithdrawalClientWrapper.requestWithdrawal(withdrawalId, accountId, address, amount));
    }

    @Test
    public void should_lock_balance_on_request() {
        var accountId = new Common.AccountId(java.util.UUID.randomUUID().toString());
        var address = new WithdrawalService.Address("testAddress");
        var amount = new Common.CurrencyAmount(java.math.BigDecimal.ONE, "USD");
        var withdrawalId = new WithdrawalService.WithdrawalId(java.util.UUID.randomUUID());

        when(accountService.accountExists(accountId)).thenReturn(true);
        asyncWithdrawalClientWrapper.requestWithdrawal(withdrawalId, accountId, address, amount);

        verify(accountService).lockBalance(accountId, amount);
    }


    @Test
    public void should_return_withdrawal_state() {
        var accountId = new Common.AccountId(java.util.UUID.randomUUID().toString());
        var address = new WithdrawalService.Address("testAddress");
        var amount = new Common.CurrencyAmount(java.math.BigDecimal.ONE, "USD");
        var withdrawalId = new WithdrawalService.WithdrawalId(java.util.UUID.randomUUID());

        when(accountService.accountExists(accountId)).thenReturn(true);
        asyncWithdrawalClientWrapper.requestWithdrawal(withdrawalId, accountId, address, amount);
        when(withdrawalService.getRequestState(withdrawalId)).thenReturn(WithdrawalService.WithdrawalState.COMPLETED);
        ArgumentCaptor<Runnable> command = ArgumentCaptor.forClass(Runnable.class);
        asyncWithdrawalClientWrapper.start();
        verify(executorService).scheduleAtFixedRate(command.capture(), anyLong(), anyLong(), any());
        command.getValue().run();
        var withdrawalState = asyncWithdrawalClientWrapper.getRequestState(withdrawalId);
        assertEquals(Common.WithdrawalState.SUCCESS, withdrawalState);
    }

    @Test
    public void should_unlock_funds_if_request_failed() {
        var accountId = new Common.AccountId(java.util.UUID.randomUUID().toString());
        var address = new WithdrawalService.Address("testAddress");
        var amount = new Common.CurrencyAmount(java.math.BigDecimal.ONE, "USD");
        var withdrawalId = new WithdrawalService.WithdrawalId(java.util.UUID.randomUUID());

        when(withdrawalService.getRequestState(any())).thenReturn(WithdrawalService.WithdrawalState.FAILED);
        when(accountService.accountExists(accountId)).thenReturn(true);

        asyncWithdrawalClientWrapper.requestWithdrawal(withdrawalId, accountId, address, amount);
        verify(accountService).lockBalance(accountId, amount);
        asyncWithdrawalClientWrapper.start();
        ArgumentCaptor<Runnable> captor = ArgumentCaptor.forClass(Runnable.class);
        verify(executorService).scheduleAtFixedRate(captor.capture(), anyLong(), anyLong(), any());
        captor.getValue().run();
        verify(accountService).unlockBalance(accountId, amount);
    }

    @Test
    public void should_remove_locked_funds_on_success() {
        var accountId = new Common.AccountId(java.util.UUID.randomUUID().toString());
        var address = new WithdrawalService.Address("testAddress");
        var amount = new Common.CurrencyAmount(java.math.BigDecimal.ONE, "USD");
        var withdrawalId = new WithdrawalService.WithdrawalId(java.util.UUID.randomUUID());

        when(withdrawalService.getRequestState(any())).thenReturn(WithdrawalService.WithdrawalState.COMPLETED);
        when(accountService.accountExists(accountId)).thenReturn(true);
        asyncWithdrawalClientWrapper.requestWithdrawal(withdrawalId, accountId, address, amount);
        asyncWithdrawalClientWrapper.start();
        ArgumentCaptor<Runnable> captor = ArgumentCaptor.forClass(Runnable.class);
        verify(executorService).scheduleAtFixedRate(captor.capture(), anyLong(), anyLong(), any());
        captor.getValue().run();
        verify(accountService).removeLockedFunds(eq(accountId), eq(amount));
    }

    @Test
    public void should_return_pending_state_if_could_not_remove_locked_funds() {
        var accountId = new Common.AccountId(java.util.UUID.randomUUID().toString());
        var address = new WithdrawalService.Address("testAddress");
        var amount = new Common.CurrencyAmount(java.math.BigDecimal.ONE, "USD");
        var withdrawalId = new WithdrawalService.WithdrawalId(java.util.UUID.randomUUID());
        
        when(withdrawalService.getRequestState(any())).thenReturn(WithdrawalService.WithdrawalState.COMPLETED);
        when(accountService.accountExists(accountId)).thenReturn(true);
        asyncWithdrawalClientWrapper.requestWithdrawal(withdrawalId, accountId, address, amount);
        asyncWithdrawalClientWrapper.start();
        ArgumentCaptor<Runnable> captor = ArgumentCaptor.forClass(Runnable.class);
        verify(executorService).scheduleAtFixedRate(captor.capture(), anyLong(), anyLong(), any());
        doThrow(new IllegalStateException()).when(accountService).removeLockedFunds(eq(accountId), eq(amount));
        captor.getValue().run();
        var withdrawalState = asyncWithdrawalClientWrapper.getRequestState(withdrawalId);
        assertEquals(Common.WithdrawalState.PENDING, withdrawalState);
    }

    @Test
    public void should_return_pending_state_if_could_not_unlock_balance() {
        var accountId = new Common.AccountId(java.util.UUID.randomUUID().toString());
        var address = new WithdrawalService.Address("testAddress");
        var amount = new Common.CurrencyAmount(java.math.BigDecimal.ONE, "USD");
        var withdrawalId = new WithdrawalService.WithdrawalId(java.util.UUID.randomUUID());

        when(withdrawalService.getRequestState(any())).thenReturn(WithdrawalService.WithdrawalState.FAILED);
        when(accountService.accountExists(accountId)).thenReturn(true);
        asyncWithdrawalClientWrapper.requestWithdrawal(withdrawalId, accountId, address, amount);
        asyncWithdrawalClientWrapper.start();
        ArgumentCaptor<Runnable> captor = ArgumentCaptor.forClass(Runnable.class);
        verify(executorService).scheduleAtFixedRate(captor.capture(), anyLong(), anyLong(), any());
        doThrow(new IllegalStateException()).when(accountService).unlockBalance(eq(accountId), eq(amount));
        captor.getValue().run();
        var withdrawalState = asyncWithdrawalClientWrapper.getRequestState(withdrawalId);
        assertEquals(Common.WithdrawalState.PENDING, withdrawalState);
    }

}
