package org.jwierzbicki.task.deposit;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.jwierzbicki.task.account.AccountService;
import org.jwierzbicki.task.Common;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.concurrent.ScheduledExecutorService;

import static org.junit.Assert.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AsyncDepositClientWrapperTest extends TestCase {

    @Mock
    ScheduledExecutorService executorService;

    @Mock
    AccountService accountService;

    @Mock
    DepositService depositService;

    private AsyncDepositClientWrapper asyncDepositClientWrapper;


    @Before
    public void setUp() {
        reset(executorService, accountService, depositService);
        asyncDepositClientWrapper = new AsyncDepositClientWrapper(accountService, depositService, executorService);
    }

    @Test
    public void should_call_deposit_if_correct_deposit() {
        // given
        var accountId = new Common.AccountId(java.util.UUID.randomUUID().toString());
        var amount = new Common.CurrencyAmount(java.math.BigDecimal.ONE, "USD");
        var depositId = new DepositService.DepositId(java.util.UUID.randomUUID().toString());
        // when
        when(accountService.accountExists(accountId)).thenReturn(true);
        // then
        asyncDepositClientWrapper.requestDeposit(depositId, accountId, amount);
        verify(depositService).requestDeposit(any(), any(), any());
    }

    @Test
    public void should_add_funds_if_request_success() {
        // given
        var accountId = new Common.AccountId(java.util.UUID.randomUUID().toString());
        var amount = new Common.CurrencyAmount(java.math.BigDecimal.ONE, "USD");
        var depositId = new DepositService.DepositId(java.util.UUID.randomUUID().toString());
        // when
        when(accountService.accountExists(accountId)).thenReturn(true);
        when(depositService.getRequestState(any())).thenReturn(DepositService.DepositState.SUCCESS);

        // then
        asyncDepositClientWrapper.requestDeposit(depositId, accountId, amount);
        ArgumentCaptor<Runnable> command = ArgumentCaptor.forClass(Runnable.class);
        asyncDepositClientWrapper.start();
        verify(executorService).scheduleAtFixedRate(command.capture(), anyLong(), anyLong(), any());
        command.getValue().run();
        verify(accountService).addFunds(accountId, amount);
    }

    @Test
    public void should_not_deposit_funds_if_account_does_not_exist() {
        // given
        var depositId = new DepositService.DepositId(java.util.UUID.randomUUID().toString());
        var accountId = new Common.AccountId(java.util.UUID.randomUUID().toString());
        var amount = new Common.CurrencyAmount(java.math.BigDecimal.ONE, "USD");
        
        // when
        when(accountService.accountExists(accountId)).thenReturn(false);

        // then
        assertThrows(IllegalArgumentException.class, () -> asyncDepositClientWrapper.requestDeposit(depositId, accountId, amount));
    }

    @Test
    public void should_not_add_funds_if_request_success() {
        // given
        var accountId = new Common.AccountId(java.util.UUID.randomUUID().toString());
        var amount = new Common.CurrencyAmount(java.math.BigDecimal.ONE, "USD");
        var depositId = new DepositService.DepositId(java.util.UUID.randomUUID().toString());

        // when
        when(accountService.accountExists(accountId)).thenReturn(true);
        when(depositService.getRequestState(any())).thenReturn(DepositService.DepositState.FAILED);

        // then
        asyncDepositClientWrapper.requestDeposit(depositId, accountId, amount);
        ArgumentCaptor<Runnable> command = ArgumentCaptor.forClass(Runnable.class);
        asyncDepositClientWrapper.start();
        verify(executorService).scheduleAtFixedRate(command.capture(), anyLong(), anyLong(), any());
        reset(accountService);
        command.getValue().run();
        verifyNoInteractions(accountService);
    }

    @Test
    public void should_return_failure_if_request_failed() {
        // given
        var accountId = new Common.AccountId(java.util.UUID.randomUUID().toString());
        var amount = new Common.CurrencyAmount(java.math.BigDecimal.ONE, "USD");
        var depositId = new DepositService.DepositId(java.util.UUID.randomUUID().toString());

        // when
        when(accountService.accountExists(accountId)).thenReturn(true);
        when(depositService.getRequestState(any())).thenReturn(DepositService.DepositState.FAILED);

        // then
        asyncDepositClientWrapper.requestDeposit(depositId, accountId, amount);
        ArgumentCaptor<Runnable> command = ArgumentCaptor.forClass(Runnable.class);
        asyncDepositClientWrapper.start();
        verify(executorService).scheduleAtFixedRate(command.capture(), anyLong(), anyLong(), any());
        command.getValue().run();
        assertEquals(DepositService.DepositState.FAILED, asyncDepositClientWrapper.getRequestState(new DepositService.DepositId(depositId.value())));
    }

    @Test
    public void should_return_pending_if_request_pending() {
        // given
        var accountId = new Common.AccountId(java.util.UUID.randomUUID().toString());
        var amount = new Common.CurrencyAmount(java.math.BigDecimal.ONE, "USD");
        var depositId = new DepositService.DepositId(java.util.UUID.randomUUID().toString());

        // when
        when(accountService.accountExists(accountId)).thenReturn(true);

        // then
        asyncDepositClientWrapper.requestDeposit(depositId, accountId, amount);
        assertEquals(DepositService.DepositState.PENDING, asyncDepositClientWrapper.getRequestState(new DepositService.DepositId(depositId.value())));
    }

    @Test
    public void should_fail_if_request_not_found() {
        // given
        var accountId = new Common.AccountId(java.util.UUID.randomUUID().toString());
        var amount = new Common.CurrencyAmount(java.math.BigDecimal.ONE, "USD");
        var depositId = new DepositService.DepositId(java.util.UUID.randomUUID().toString());

        // when
        when(accountService.accountExists(accountId)).thenReturn(true);
        when(depositService.getRequestState(any())).thenThrow(new IllegalArgumentException());

        // then
        asyncDepositClientWrapper.requestDeposit(depositId, accountId, amount);
        ArgumentCaptor<Runnable> command = ArgumentCaptor.forClass(Runnable.class);
        asyncDepositClientWrapper.start();
        verify(executorService).scheduleAtFixedRate(command.capture(), anyLong(), anyLong(), any());
        command.getValue().run();
        assertEquals(DepositService.DepositState.FAILED, asyncDepositClientWrapper.getRequestState(new DepositService.DepositId(depositId.value())));
    }

    @Test
    public void should_do_nothing_if_request_is_not_finished() {
        // given
        var accountId = new Common.AccountId(java.util.UUID.randomUUID().toString());
        var amount = new Common.CurrencyAmount(java.math.BigDecimal.ONE, "USD");
        var depositId = new DepositService.DepositId(java.util.UUID.randomUUID().toString());

        // when
        when(accountService.accountExists(accountId)).thenReturn(true);
        when(depositService.getRequestState(any())).thenReturn(DepositService.DepositState.PENDING);

        // then
        asyncDepositClientWrapper.requestDeposit(depositId, accountId, amount);
        reset(accountService);
        ArgumentCaptor<Runnable> command = ArgumentCaptor.forClass(Runnable.class);
        asyncDepositClientWrapper.start();
        verify(executorService).scheduleAtFixedRate(command.capture(), anyLong(), anyLong(), any());
        command.getValue().run();
        verifyNoInteractions(accountService);
    }

    @Test
    public void should_leave_request_in_pending_state_if_cannot_add_funds() {
        // given
        var accountId = new Common.AccountId(java.util.UUID.randomUUID().toString());
        var amount = new Common.CurrencyAmount(java.math.BigDecimal.ONE, "USD");
        var depositId = new DepositService.DepositId(java.util.UUID.randomUUID().toString());

        // when
        when(accountService.accountExists(accountId)).thenReturn(true);
        when(depositService.getRequestState(any())).thenReturn(DepositService.DepositState.SUCCESS);

        // then
        asyncDepositClientWrapper.requestDeposit(depositId, accountId, amount);
        reset(accountService);
        ArgumentCaptor<Runnable> command = ArgumentCaptor.forClass(Runnable.class);
        asyncDepositClientWrapper.start();
        verify(executorService).scheduleAtFixedRate(command.capture(), anyLong(), anyLong(), any());
        doThrow(new IllegalStateException()).when(accountService).addFunds(any(), any());
        command.getValue().run();
        assertEquals(DepositService.DepositState.PENDING, asyncDepositClientWrapper.getRequestState(new DepositService.DepositId(depositId.value())));
    }

}